//Llamamos al boton "Enviar" y lo guardamos en la variable "enviar".
let enviar=document.querySelector("button");
//Le asignamos al boton la funcion "validar", cuando hacemos "click" en el boton.
enviar.addEventListener("click", validar);
//llamamos al h3.cerrar para usarlo como boton para cerrar el modal.
let cerrar = document.querySelector(".cerrar");
//Le asignamos al h3.cerrar la funcion "modal", cuando hacemos "click" en el.
cerrar.addEventListener("click", modal);
//Variable "bandera", para saber si el modal esta oculto o no. "true" significa que esta oculto y "false" significa que no.
let oculto = true;
//Llamamos al contenedor del mensaje del modal, para hacer que cambie de color.
let contenedor = document.querySelector(".contenedor");


//Definimos la funcion validar.
function validar()
	{		
		//Llamamos a todos los datos de los inputs y los guardamos a cada uno en una variable.
		let nom = document.querySelector("#nombre").value;
		let ape = document.querySelector("#apellido").value;
		let edad = parseInt(document.querySelector("#edad").value);
		let lic1 = document.querySelector("#licencia1").checked;
		let lic2 = document.querySelector("#licencia2").checked;
		let fec = document.querySelector("#fecha").value;
		let titulo = document.querySelector(".titulo-men");
		let mensaje = document.querySelector(".mensaje");
		let op = 0;

		if(fec=="")
			{
				op=5;
			}
		else
			{
				let fecha_actual = new Date;
				//guardamos primero el año, junto con un guion.
				let hoy = fecha_actual.getFullYear()+"-";

				//Si el mes es menor a 10, se le agrega un cero en frente y un guion despues.
				if(fecha_actual.getMonth()<10)
					{
						hoy += "0"+fecha_actual.getMonth()+"-"; 
					}
				else
					{
						hoy += fecha_actual.getMonth()+"-"; 
					}

				//Si el dia es menor a 10, se le agrega un cero en frente y un guion despues.
				if(fecha_actual.getDate()<10)
					{
						hoy += "0"+fecha_actual.getDate()+"-";	
					}
				else
					{
						hoy += fecha_actual.getMonth();
					}

				if(fec > hoy)
					{
						op=7;
					}
				else
					{
						op=6;
					}
			}

		if(lic1==false)
			{
				op=4;
			}

		if(edad>=18)
			{
				//solo para que no tener problemas con la conversion a NaN.
			}
		else
			{
				op=3;
			}

		if(ape=="")
			{
				op=2;
			}

		if(nom=="")
			{
				op=1;
			}


		switch(op)
			{
				case 1:
					titulo.innerHTML="Hubo un error."
					mensaje.innerHTML="Ud. no completo su nombre.";
					//Le da la clase "negativo" al contenedor, que hace que tenga el fondo rojo.
					contenedor.classList.add("negativo");
					//funcion que hace que se abra el modal despues de completar el formulario.
					modal();
					break;

				case 2:
					titulo.innerHTML="Hubo un error."
					mensaje.innerHTML="Ud. no completo su apellido.";
					//Le da la clase "negativo" al contenedor, que hace que tenga el fondo rojo.
					contenedor.classList.add("negativo");
					//funcion que hace que se abra el modal despues de completar el formulario.
					modal();
					break;

				case 3:
					titulo.innerHTML="Hubo un error."
					mensaje.innerHTML="Ud. es menor o no completo su edad.";
					//Le da la clase "negativo" al contenedor, que hace que tenga el fondo rojo.
					contenedor.classList.add("negativo");
					//funcion que hace que se abra el modal despues de completar el formulario.
					modal();
					break;

				case 4:
					titulo.innerHTML="Hubo un error."
					mensaje.innerHTML="Ud. admitio no tener licencia.";
					//Le da la clase "negativo" al contenedor, que hace que tenga el fondo rojo.
					contenedor.classList.add("negativo");
					//funcion que hace que se abra el modal despues de completar el formulario.
					modal();
					break;

				case 5:
					titulo.innerHTML="Hubo un error."
					mensaje.innerHTML="Ud. no completo la fecha de expiracion.";
					//Le da la clase "negativo" al contenedor, que hace que tenga el fondo rojo.
					contenedor.classList.add("negativo");
					//funcion que hace que se abra el modal despues de completar el formulario.
					modal();
					break;

				case 6:
					titulo.innerHTML="Hubo un error."
					mensaje.innerHTML="La fecha de su permiso expiro!";
					//Le da la clase "negativo" al contenedor, que hace que tenga el fondo rojo.
					contenedor.classList.add("negativo");
					//funcion que hace que se abra el modal despues de completar el formulario.
					modal();
					break;

				case 7:
					titulo.innerHTML="Completado exitosamente!"
					mensaje.innerHTML="Felicidades "+nom+" "+ape+", vos si que podes pistear como un campeon!";
					//Le quita la clase "negativo" al contenedor.
					contenedor.classList.remove("negativo");
					//Le da la clase "positivo" al contenedor, que hace que tenga el fondo verde.
					contenedor.classList.add("positivo");
					//funcion que hace que se abra el modal despues de completar el formulario.
					modal();
					break;
			}
	}


//Funcion que hace que el modal aparezca o desaparezca.
function modal()
	{
		let mostrar = document.querySelector(".modal");		
		if(oculto==true)
			{
				//Hace que el modal se vea, porque le quita la clase oculto.
				mostrar.classList.remove("oculto");
				oculto=false;	

			}
		else
			{
				//Hace que el modal no se vea, porque le agrega la clase oculto.
				mostrar.classList.add("oculto");				
				oculto=true;
			}
	}